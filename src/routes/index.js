import { lazy } from "react";

const AppRoutes = [
  {
    name: "homepage",
    path: "/",
    component: lazy(() => import("../pages/HomePage")),
    exact: true,
  },
];

export { AppRoutes };
